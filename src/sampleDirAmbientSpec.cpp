#include "sampleDirAmbientSpec.h"

class SingleMaterialProvider : public wolf::Model::MaterialProvider
{
public:
    SingleMaterialProvider(const std::string& matName) : m_matName(matName) { }

    wolf::Material* getMaterial(const std::string& nodeName, int subMeshIndex, const std::string& name) const override
    {
        // Regardless of what mesh index or mat name the model wants, we just
        // use the mat we were seeded with. Note that we create a new one each
        // time so the DestroyMaterial calls line up. This could be improved,
        // but they do share shaders.
        return wolf::MaterialManager::CreateMaterial(m_matName);
    }

private:
    std::string m_matName;
};

SampleDirAmbientSpec::~SampleDirAmbientSpec()
{
	printf("Destroying Directional + Ambient Sample\n");
    wolf::MaterialManager::DestroyMaterial(m_pMat);
    wolf::TextureManager::DestroyTexture(m_pTex);
    delete m_pModel;
    delete m_pGrid;
    delete m_pOrbitCam;
}

void SampleDirAmbientSpec::init()
{
	// Only init if not already done
    if(!m_pModel)
    {
        m_pTex = wolf::TextureManager::CreateTexture("data/diffuseGray.png");
        m_pTex->SetWrapMode(wolf::Texture::WM_Repeat);

        const std::string MATNAME = "directionalAmbSpec";
        m_pMat = wolf::MaterialManager::CreateMaterial(MATNAME);
        m_pMat->SetProgram("data/dirAmbientSpec.vsh", "data/dirAmbientSpec.fsh");
        m_pMat->SetDepthTest(true);
        m_pMat->SetDepthWrite(true);

        m_pMat->SetUniform("u_lightDir", glm::vec3(0.0f,0.0f,1.0f));
        m_pMat->SetUniform("u_lightColor", glm::vec3(1.0f,1.0f,0.0f));
        m_pMat->SetUniform("u_ambientLight", glm::vec3(0.3f,0.1f,0.1f));
        m_pMat->SetUniform("u_diffuseTex", 0);
        m_pMat->SetUniform("u_specularColor", glm::vec3(1.0f,1.0f,1.0f));

        SingleMaterialProvider matProvider(MATNAME);
        m_pModel = new wolf::Model("data/unreal.fbx", matProvider);

        glm::vec3 min = m_pModel->getAABBMin();
        glm::vec3 max = m_pModel->getAABBMax();
        glm::vec3 center = m_pModel->getAABBCenter();

        m_pOrbitCam = new OrbitCamera(m_pApp);
        m_pOrbitCam->focusOn(min,max);

        float gridSize = 2.5f * wolf::max(max.x,max.z);
        m_pGrid = new Grid3D(10, gridSize / 10.0f);
        m_pGrid->hideAxes();
    }

    printf("Successfully initialized Directional + Ambient Sample\n");
}

void SampleDirAmbientSpec::update(float dt) 
{
    m_pOrbitCam->update(dt);
    m_pGrid->update(dt);
}

void SampleDirAmbientSpec::render(int width, int height)
{
	glClearColor(0.3f, 0.3f, 0.3f, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::mat4 mProj = m_pOrbitCam->getProjMatrix(width, height);
	glm::mat4 mView = m_pOrbitCam->getViewMatrix();
	glm::mat4 mWorld = glm::rotate(glm::mat4(1.0f), m_rot, glm::vec3(0.0f, 1.0f, 0.0f));

    m_pGrid->render(mView, mProj);

    if(m_pApp->isKeyDown('w'))
        m_wKeyDown = true;
    else if(m_wKeyDown)
    {
        m_wKeyDown = false;
        m_currShininess += 10.0f;
        printf("Changed shininess to %f\n", m_currShininess);
    }

    if(m_pApp->isKeyDown('s'))
        m_sKeyDown = true;
    else if(m_sKeyDown)
    {
        m_sKeyDown = false;
        m_currShininess -= 10.0f;
        printf("Changed shininess to %f\n", m_currShininess);
    }

    m_pMat->SetUniform("u_viewPos", m_pOrbitCam->getViewPosition());
    m_pMat->SetUniform("u_shininess", m_currShininess);

    m_pTex->Bind();

    m_pModel->Render(mWorld, mView, mProj);
}

