#include <stdio.h>
#include <iostream>
#include <glm/glm.hpp>
#include "../wolf/wolf.h"
#include "../samplefw/SampleRunner.h"
#include "sampleDirectional1.h"
#include "sampleDirectionalWithMat.h"
#include "sampleDirectionalWithTex.h"
#include "sampleDirectional2.h"
#include "sampleDirAmbient.h"
#include "sampleDirAmbientSpec.h"

class Week2: public wolf::App
{
public:
    Week2() : wolf::App("Week 4")
    {
        m_sampleRunner.addSample(new SampleDirectional1(this));
        m_sampleRunner.addSample(new SampleDirectional2(this));
        m_sampleRunner.addSample(new SampleDirectionalWithMat(this));
        m_sampleRunner.addSample(new SampleDirectionalWithTex(this));
        m_sampleRunner.addSample(new SampleDirAmbient(this));
        m_sampleRunner.addSample(new SampleDirAmbientSpec(this));
    }

    ~Week2()
    {
    }

    void update(float dt) override
    {
        if(isKeyDown(' '))
        {
            m_lastDown = true;
        }
        else if(m_lastDown)
        {
            m_sampleRunner.nextSample();
            m_lastDown = false;
        }

        m_sampleRunner.update(dt);
    }

    void render() override
    {
        m_sampleRunner.render(m_width, m_height);
    }

private:
    SampleRunner m_sampleRunner;
    bool m_lastDown = false;
};

int main(int, char**) {
    Week2 week2;
    week2.run();
}