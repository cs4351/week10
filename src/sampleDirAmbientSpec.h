#pragma once
#include "../wolf/wolf.h"
#include "../samplefw/Sample.h"
#include "../samplefw/Grid3D.h"
#include "../samplefw/OrbitCamera.h"

class SampleDirAmbientSpec: public Sample
{
public:
    SampleDirAmbientSpec(wolf::App* pApp) : Sample(pApp,"Directional Light with Ambient and Specular added") {}
    ~SampleDirAmbientSpec();

    void init() override;
    void update(float dt) override;
    void render(int width, int height) override;

private:
    wolf::Model* m_pModel = nullptr;
    wolf::Material* m_pMat = nullptr;
    wolf::Texture* m_pTex = nullptr;
    Grid3D* m_pGrid = nullptr;
    OrbitCamera* m_pOrbitCam = nullptr;
    float m_rot = 0.0f;
    float m_currShininess = 200.0f;
    float m_wKeyDown = false;
    float m_sKeyDown = false;
};
